const path = require('path')
const webpack = require('webpack')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const browserConfig = {
    entry: [
        'babel-polyfill',
        './src/browser/index'
    ],
    output: {
        path: path.resolve(__dirname,'dist/assets/'),
        filename: 'bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: '/node_modules/'
            },{
                test: /\.(gif|jpe?g|png|ico)$/,
                loader: 'url-loader?limit=10000'
            },
            {
                test: /\.(otf|eot|svg|ttf|woff|woff2).*$/,
                loader: 'url-loader?limit=10000'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: true
        }),
        //new BundleAnalyzerPlugin()
    ]
}

const serverConfig = {
    entry: './src/serve/index',
    target: 'node',
    output: {
        path: path.resolve(__dirname,'dist/'),
        filename: 'launcher.js',
        publicPath: '/'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: '/node_modules/'
            },{
                test: /\.(gif|jpe?g|png|ico)$/,
                loader: 'url-loader?limit=10000'
            },
            {
                test: /\.(otf|eot|svg|ttf|woff|woff2).*$/,
                loader: 'url-loader?limit=10000'
            }
        ]
    },
    mode: 'development',
    plugins: [
        new webpack.DefinePlugin({
            __isBrowser__: false
        })
    ]
}

module.exports = [
    browserConfig,
    serverConfig
]