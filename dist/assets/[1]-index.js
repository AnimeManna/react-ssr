import React from 'react'
import {
    hydrate
} from 'react-dom'
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles'
import {BrowserRouter, HashRouter} from 'react-router-dom'
import blue from '@material-ui/core/colors/blue'

import Launcher from '../shared/index'

class Main extends React.Component {
    /**
     * Удаляем стили, которые были инжектированы с серверной стороны
     */
    componentDidMount() {
        const jssStyles = document.getElementById('jss-server-side')
        if (jssStyles && jssStyles.parentNode) {
            jssStyles.parentNode.removeChild(jssStyles)
        }
    }

    render() {
        return <Launcher/>
    }
}

const theme = createMuiTheme({
    palette: {
        primary: {
            main: blue[500]
        }
    }
})


hydrate(
    <BrowserRouter>
        <MuiThemeProvider
            theme={theme}
        >
            <Main/>
        </MuiThemeProvider>
    </BrowserRouter>,
    document.querySelector('#content')
)