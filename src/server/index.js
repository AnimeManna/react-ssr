import express from 'express'
import React from 'react'
import {MongoClient} from 'mongodb'
import bodyParser from 'body-parser'
import cors from 'cors
import test from './test'



import handleRender from './handleRender'

const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true

}))

const mongoUrl = "mongodb://localhost:27017/";


app.use(express.static('dist/assets/'))
app.use(handleRender) // Рендерим страницу каждый раз когда выполняется запрос


MongoClient.connect(mongoUrl, (err, client)=>{
   test(app)
    app.listen('3000', () => {
        console.log('Server listening 3000 port. Go to "localhost:3000"')
    })
})