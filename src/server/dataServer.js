const express = require('express');
const bodyParser = require('body-parser');
const mongoClient = require("mongodb").MongoClient;
const cors = require('cors');
const app = express();


const url = "mongodb://localhost:27017/";

app.use(bodyParser.json())
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true

}))


mongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    const db = client.db("product");
    app.get('/:products', async (req, res) => {
        const uri = req.params.products;
        console.log(uri);
        const collection = db.collection(uri);
        let data = await collection.find().toArray();
        res.send(data);
    })

    app.get('/:products/:id', async (req, res) => {
        console.log(req.params)
        const {products, id} = req.params
        const collection = db.collection(products)
        let data = await collection.findOne({"_id": id})
        res.send(data);
    })

    app.post('/cart', async(req,res)=>{
        const collection = db.collection("users");
        const email = req.body.user
        console.log('EMAIL===>>',email);
        let userProducts = await collection.findOne({"email":email});
        console.log('PRODUCTS ========>',userProducts.products);
        res.send({
            products:userProducts.products,
            errorMessage:'А вот и твои продукты',
            success:true
        })
    })

    app.post('/shop', async (req, res) => {
        const {user} = req.body
        const {email} = req.body.user
        const collection = db.collection(user.uri.products);
        let products = await collection.findOne({"_id": user.uri.id});
        const userCollection = db.collection("users");
        let newUserData = await userCollection.findOneAndUpdate({"email": email}, {$addToSet: {"products": products}});
        console.log(newUserData)
        res.send(
            {
                errorMessage: 'Успешно добавленно',
                success: true
            }
        );
    })

    app.post('/account', async (req, res) => {
        console.log(req.body.user)
        const collection = db.collection("users");
        const {user} = req.body
        let checkUser = await collection.findOne({"email": user});
        let success = false;
        let userInfo = {};
        if (!!checkUser) {
            userInfo = checkUser;
            success = true;
        } else {
            userInfo = undefined;
            success = false
        }
        res.send({
            data: userInfo,
            success: success
        })
    })

    app.post('/changeAccount', async (req, res) => {
        console.log(req.body.user)
        const {name, data} = req.body.user
        const collection = db.collection("users");
        let acceptAccount = await collection.findOne({[name]: data});
        let success = false;
        let errorMessage = '';
        if (!!acceptAccount) {
            success = false;
            errorMessage = 'Кажется такой молодой человек у нас уже есть ';
            switch (name) {
                case 'login': {
                    errorMessage = 'Кажется историю человека с таким именем мы уже записали, ты уверен что именно так тебя звали?';
                    break;
                }
                case 'number': {
                    errorMessage = 'Хмм, поискав в своей телефоной книжке я нашёл такой номер, ты уверен что у тебя именно такой номер?';
                    break;
                }
                case 'email': {
                    errorMessage = 'Оу слушай, а ведь на такой адресс я уже высылал письма, уверен что это твой адресс?'
                    break;
                }
            }
        } else {
            success = true;
            errorMessage = 'Оу так вот как всё было, мы запишем'
        }
        res.send({
            success,
            errorMessage
        })
    })

    app.post('/login', async (req, res) => {
        console.log(req.body);
        const collection = db.collection("users");
        const {login, password} = req.body.user;
        let userName = collection.findOne({"login": login});
        let userPassword = collection.findOne({"password": password});
        let user = await userName;
        let pass = await userPassword
        console.log(user);
        let msg = '';
        let success = false;
        let userData = {}
        if (!user) {
            userData = undefined
            msg = 'Кажется вас нет в списке'
            success = false;
        } else if (pass) {
            userData = user;
            msg = 'Как же я мог сразу тебя не узнать, проходи, проходи!'
            success = true
        } else {
            userData = undefined;
            msg = 'Кажется это не то секретное слово'
            success = false
        }
        res.send({
            data: userData,
            msg: msg,
            success: success
        })
    })

    app.post('/register', async (req, res) => {
        console.log(req.body.user);
        const collection = db.collection("users");
        const {email, login} = req.body.user
        const userEmail = collection.findOne({"email": email});
        const userLogin = collection.findOne({login});
        const checkEmail = await userEmail;
        const checkLogin = await userLogin;
        let loginSuccess = true;
        let emailSuccess = true;
        let registerMsg = '';
        if (checkLogin) {
            loginSuccess = false;
            registerMsg = 'Припоминаю это имя, мы точно ещё не знакомы?'
        } else if (checkEmail) {
            emailSuccess = false;
            registerMsg = 'Оу, кажется на этот адресс я уже отсылал письма, так мы знакомы?'
        } else {
            collection.insertOne(req.body.user)
            registerMsg = 'Мне было очень приятно познакомится, проходи'
        }
        res.send({
            emailSuccess,
            loginSuccess,
            registerMsg
        })
    })

});


app.listen(8080, () => {
    console.log('Вы подключились к localhost:8080')
});
