import express from 'express'
import cors from 'cors'
import Test from './test'

const app = express();

app.use(cors());
Test(app);

app.listen(2000,()=>{
    console.log('Okey go 2000 port')
})