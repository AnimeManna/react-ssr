export default (content, css) => {
    return (`
        <html>
            <head>
                <title>React-miracle</title>
                <script src="bundle.js" defer></script>
               <link rel="icon" href="https://cdn.auth0.com/blog/optimizing-react/logo.png" type="image/x-icon">
            </head>
            <body>
                <div id="content">
                    <!--Подключение потрохов-->
                    ${content}
                </div>
                <!--Подключение стилей JSS-->
                <style id="jss-server-side">${css}</style>
            </body>
        </html>
    `)
}