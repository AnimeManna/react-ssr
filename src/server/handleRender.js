import React from 'react'
import { renderToString } from 'react-dom/server'
import { SheetsRegistry } from 'react-jss/lib/jss'
import JssProvider from 'react-jss/lib/JssProvider'
import {
    MuiThemeProvider,
    createMuiTheme,
    createGenerateClassName,
} from '@material-ui/core/styles'
import green from '@material-ui/core/colors/green'
import blue from '@material-ui/core/colors/blue'

import renderFullPage from './renderFullPage'
import Launcher from '../shared/index.js'

export default (req, res) => {
    const sheetsRegistry = new SheetsRegistry()

    const theme = createMuiTheme({
        palette: {
            primary: {
                main: blue[500]
            },
            secondary: {
                main: '#11cb5f'
            }
        }
    })

    const generateClassName = createGenerateClassName()

    const html = renderToString(
        <JssProvider
            registry={sheetsRegistry}
            generateClassName={generateClassName}
        >
            <MuiThemeProvider
                theme={theme}
                sheetsManager={new Map()}
            >
                <Launcher/>
            </MuiThemeProvider>
        </JssProvider>
    )
    const css = sheetsRegistry.toString()
    res.send(renderFullPage(html, css))
}