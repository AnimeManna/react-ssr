import React, {
    Component,
    Fragment
} from 'react'


import MenuIcon from '@material-ui/icons/Menu';
import {
    withStyles
} from '@material-ui/core/styles'
import {
    Typography,
    Toolbar,
    CssBaseline,
    AppBar,
    Collapse,
    Drawer,
    IconButton
} from '@material-ui/core'

import Sidebar from './components/Sidebar'
import Content from './components/Content'
import UserMenu from './components/AppBarComponent/UserMenu'
import LoginIcon from './components/AppBarComponent/LoginIcon'


const styles = theme => ({
    root: {
        flexGrow: 1,
        minHeight: 630,
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        textDecoration: 'none'
    },

    AppBar: {
        margin: 0,
        zIndex: theme.zIndex.drawer + 1
    },
    Content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        minWidth: 0,
    },
    drawerPaper: {
        position: 'relative',
        width: 250,
    },
    toolbar: theme.mixins.toolbar,
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    }, flex: {
        flexGrow: 1,
    }

})

class Launcher extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenSidebar: false,
            isAuth: false,
            user:{
                img:'',
                login:'',
                email:'',
                products:{}
            }
        }
        this.changeSidebar = this.changeSidebar.bind(this);
        this.changeAuth=this.changeAuth.bind(this);
        this.exitAccount=this.exitAccount.bind(this)
    }

    changeAuth(authState, userData) {
        this.setState({
            isAuth: authState,
            user:{
                ...userData
            }
        })
    }

    exitAccount(exitState){
        this.setState({
            isAuth:exitState
        })
    }

    changeSidebar() {
        this.setState({
            isOpenSidebar: !this.state.isOpenSidebar
        })
    }


    render() {
        const classes = this.props.classes;
        return (
            <CssBaseline>
                <Fragment>
                    <div className={classes.root}>
                        <AppBar
                            position='absolute'
                            className={classes.AppBar}
                        >
                            <Toolbar>
                                <IconButton color="inherit" onClick={this.changeSidebar} className={classes.menuButton}>
                                    <MenuIcon/>
                                </IconButton>
                                <Typography variant="title" color="inherit" className={classes.flex}>
                                    Creative Shop
                                </Typography>
                                {this.state.isAuth
                                    ? <UserMenu userData ={this.state.user} exitAccount={(exitStatus)=>{
                                    this.exitAccount(exitStatus)}
                                    } />
                                    : <LoginIcon/>
                                }

                            </Toolbar>
                        </AppBar>
                        <Collapse in={this.state.isOpenSidebar} timeout="auto" unmountOnExit>
                            <Drawer
                                variant="permanent"
                                classes={{
                                    paper: classes.drawerPaper,
                                }}
                            >
                                <div className={classes.toolbar}/>
                                <Sidebar />
                            </Drawer>
                        </Collapse>
                        <main className={classes.Content}>
                            <div className={classes.toolbar}/>
                            <Content changeAuth={(authState,userData)=>{
                                this.changeAuth(authState, userData)
                            }} email={this.state.user.email} isAuth = {this.state.isAuth}
                            />
                        </main>
                    </div>
                </Fragment>
            </CssBaseline>
        )
    }
}

export default withStyles(styles)(Launcher)