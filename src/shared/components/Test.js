import React from 'react'
import fetchProviders from '../providers/fetchProviders'

class Test extends React.Component{

    constructor(props){
        super(props)
        this.state={
            items:[

            ]
        }
    }

    async createNormalUri(){
        let uri=this.props.match.url.slice(1);
        console.log(uri)
        let data = await fetchProviders.getRequest(uri);
        console.log('CREATE NORMAL URI !!!!',data);
        this.setState({
            items:data
        })
    }

    componentDidMount(){
        this.createNormalUri();
    }

    render(){
        console.log(this.props.match.url)
        return(<div onClick={()=>{
        console.log(this.state)
        }
        }>
            Hello Test
        </div>)
    }
}

export default Test