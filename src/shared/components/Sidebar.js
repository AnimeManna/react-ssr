import React from 'react'
import {
    withStyles
} from '@material-ui/core/styles'

import {
    Link
} from 'react-router-dom'

import {
    Collapse,
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Badge
} from '@material-ui/core'

import {
    Smartphone,
    Home,
    PlaylistAdd,
    ExpandLess,
    ExpandMore,
    ShoppingCart,
    Keyboard,
    Mouse
} from '@material-ui/icons'

const styles = theme => ({
    root: {
        maxWidth: 300,
        height: 550,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    sidebar__button: {
        textDecoration: 'none'
    },
    badge: {
        top: 1,
        right: -15,
        // The border color match the background color.
        border: `2px solid ${
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
            }`,
    }

})

class Sidebar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            score: 0
        }
        this.viewProducts = this.viewProducts.bind(this)
    }

    viewProducts() {
        this.setState({
            open: !this.state.open,
        })
    }


    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <List
                    component="nav">
                    <Link to='/' className={classes.sidebar__button}>
                        <ListItem button>
                            <ListItemIcon>
                                <Home/>
                            </ListItemIcon>
                            <ListItemText inset primary="Home"/>
                        </ListItem>
                    </Link>
                    <Divider/>
                    <ListItem button onClick={this.viewProducts}>
                        <ListItemIcon>
                            <PlaylistAdd/>
                        </ListItemIcon>
                        <ListItemText inset primary="Products"/>
                        {this.state.open ? <ExpandLess/> : <ExpandMore/>}
                    </ListItem>
                    <Divider/>
                    <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                        <List
                            component="div">
                            <Link to='/phone' className={classes.sidebar__button}>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon button>
                                        <Smartphone/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Phone"/>
                                </ListItem>
                            </Link>
                            <Link to='/keyboard' className={classes.sidebar__button}>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <Keyboard/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Keyboard"/>
                                </ListItem>
                            </Link>
                            <Link to='/mouse' className={classes.sidebar__button}>
                                <ListItem button className={classes.nested}>
                                    <ListItemIcon>
                                        <Mouse/>
                                    </ListItemIcon>
                                    <ListItemText inset primary="Mouse"/>
                                </ListItem>
                            </Link>
                        </List>
                    </Collapse>
                    <Divider/>
                    <Link to='/cart' className={classes.sidebar__button}>
                        <ListItem button>
                            <ListItemIcon>
                                {
                                    this.state.score > 0
                                        ? (<Badge
                                            badgeContent={this.state.score}
                                            color="primary"
                                            classes={{badge: classes.badge}}
                                        >
                                            <ShoppingCart/>
                                        </Badge>)
                                        : <ShoppingCart/>
                                }
                            </ListItemIcon>
                            <ListItemText inset primary="Cart"/>
                        </ListItem>
                        <Divider/>
                    </Link>
                </List>

            </div>
        )
    }
}

export default withStyles(styles)(Sidebar)
