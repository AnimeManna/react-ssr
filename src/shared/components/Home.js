import React from 'react'
import {
    withStyles
} from '@material-ui/core/styles'

const styles = theme =>({
    root:{
        display:'flex',
        justify:'center'
    }
})

class Home extends React.Component{
    render(){
        const {classes} = this.props
        return(
            <div
                className={classes.root}
            >
                Hello in my Home mister
            </div>
        )
    }
}

export default withStyles(styles)(Home)