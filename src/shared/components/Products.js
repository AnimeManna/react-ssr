import React from 'react'
import {Link} from 'react-router-dom'
import fetchProviders from '../providers/fetchProviders'


import {
    withStyles
} from '@material-ui/core/styles';

import {
    Grid,
    GridList,
    Card,
    CardContent,
    CardMedia,
    CardActions,
    Typography,
    Button,
    Divider
} from '@material-ui/core'

const styles = theme => ({
    root: {
        flexGrow:1,
        height:"85%"
    },
    gridList: {
        width: '100%',
        height: '100%'
    },
    grid: {
        display: 'flex',
        flexWrap: 'wrap',
        flexGrow:1
    },
    card: {
        minWidth:200,
        maxWidth: 250,
        marginBottom:'10%'
    },
    cardMedia: {
        height: 300,
        backgroundSize: '100%'
    },
    Products__button:{
        textDecoration:'none'
    }
});

class Products extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: []
        }
        console.log('CONSTRUCTOR!!!');

    }

    async createRequest(props) {
        let uri = props.match.url;
        console.log('REQUEST URIII!!', uri)
        let data = await fetchProviders.getRequest(uri);
        console.log('CREATE REQUEST!!!', data);
        this.setState({
            items: data
        })
    }

    componentDidMount() {
        this.createRequest(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.createRequest(nextProps)
    }


    render() {
        const {classes} = this.props;
        console.log('im here');
        console.log(this.state);
        console.log(this.props.match.url)
        const url = this.props.match.url

        return (
            <div className={classes.root}>
                <Grid container  spacing={24} className={classes.grid}>lassName={classes.cardMedia}
                    <GridList cellHeight={400} className={classes.gridList}>
                        {this.state.items.map((product) => {
                            return <Grid item xs={3} key={product._id}>
                                <Card className={classes.card}>
                                    <CardMedia
                                        c
                                        image={product.img}
                                    />
                                    <Divider/>
                                    <CardContent>
                                        <Typography component="h1">
                                            {product.name}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Link className={classes.Products__button} to={`${this.props.match.url}/${product._id}`}>
                                            <Button size="small" color="primary">
                                                Read more
                                            </Button>
                                        </Link>
                                    </CardActions>
                                </Card>
                            </Grid>
                        })}
                    </GridList>
                </Grid>
            </div>
        )
    }
}

export default withStyles(styles)(Products)