import React from 'react'
import {
    withStyles
} from '@material-ui/core/styles'

import {
    Button,
    Typography,
    Paper
} from '@material-ui/core'
import {
    Add
} from '@material-ui/icons'

import defaultImg from '../../../dist/assets/defaultImg.png'

import fetchProviders from '../providers/fetchProviders'

const styles = theme => ({
    root: {
        width: 800,
        margin: 0,
        flexGrow: 1,
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexWrap: 'wrap',
    },
    img: {
        width: "35%",
        height: 300,
        marginRight: 20,
        border:'1px,solid,gray'
    },
    content: {
        width: "60%",

    },
    title: {
        fontFamily: "Typo Hoop"
    },
    footer: {
        width: '100%',
        display: 'flex',
        justify: 'center'
    },
    button: {
        margin: theme.spacing.unit,
    }
})

class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            img: defaultImg,
            name: 'Я есть что-то!',
            _id: '0',
            description: 'Ох чёрт описание сбежало, но не волнуйтесь мы уже выслали наряд байтов на их поиск!',
            errorMessage:'',
            success:false
        }
        this.addProduct=this.addProduct.bind(this)
    }

    async recevingData(props) {
        const uri = props.match.url;
        let newData = await fetchProviders.getRequest(uri);
        console.log(newData)
        this.setState({
            ...newData
        })

    }
    async addProduct(){
        let sendData = {
            _id:this.state._id,
            uri:this.props.match.params,
            email:this.props.email
        };
        if(this.props.isAuth){
            let newData = await fetchProviders.getUser(sendData, '/shop');
            const {errorMessage, success} = newData
            this.setState({
                errorMessage,
                success
            })
            console.log(newData);
        }else{
            this.setState({
                errorMessage:'У нас тут товар для своих для начала представся',
                success:true
            })
        }
    }

    componentDidMount() {
        this.recevingData(this.props);
    }

    render() {
        const {
            classes
        } = this.props;
        const {
            name,
            img,
            _id,
            description
        } = this.state;
        console.log(this.state);
        console.log(this.props)
        return (
            <Paper
                className={classes.root}
                elevation={1}
                key={_id}
            >
                <img
                    src={img}
                    className={classes.img}
                />
                <div
                    className={classes.content}>
                    <Typography
                        variant="headline"
                        component="h3"
                        className={classes.title}
                    >
                        Название: {name}
                    </Typography>
                    <Typography
                        component="p"
                    >
                        Описание: {description}
                    </Typography>
                </div>
                <div className={classes.footer}>
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.addProduct}>
                        Add to cart
                    </Button>
                    <Typography
                    component="p">
                        {this.state.success
                            ?this.state.errorMessage
                            :null}
                    </Typography>
                </div>
            </Paper>
        )
    }
}

export default withStyles(styles)(Product)