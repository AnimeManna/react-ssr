import React from 'react'

import fetchProviders from '../../providers/fetchProviders'
import verificationInput from '../../providers/verificationInput'

import {
    withStyles
} from '@material-ui/core/styles'

import {
    Paper,
    Typography,
    TextField,
    Button
} from '@material-ui/core'

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center' // Делаем выравнивание по центру
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2, // Создаем правильные отступы у компонента Paper
        maxWidth: 300,
        height: 500,
        display: 'flex',
        justifyContent: 'center', // Делаем поля по центру компоненты используем именно justifyContent потому что TextField Это компонент, а не текст
        flexWrap: "wrap", // Для правильного переноса, 1 поле на всю ширину
    },
    paper__typography: {
        textAlign: 'center' // Выравниваем отдельно текст в компонентах
    },
    textInput: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
        height: 60
    },
    button: {
        margin: theme.spacing.unit,
        marginBot: 20
    }
});


class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                login: '',
                password: '',
                email: '',
                number: '',
                img: ''
            },
            registerCheck: {
                loginInput: false,
                passwordInput: false,
                emailInput: false
            },
            errorInputMessage: ''
        };
        this.changeInput = this.changeInput.bind(this);
        this.checkInput = this.checkInput.bind(this);
        this.sendUserInfo = this.sendUserInfo.bind(this);
        this.checkSomeInput = this.checkSomeInput.bind(this);
    }

    changeInput(event) {
        const {name, value} = event.target;
        this.setState({
            user: {
                ...this.state.user,
                [name]: value

            }
        })
    }

    checkSomeInput(nameInput) {
        let successInput = verificationInput.checkInput(this.state.user[nameInput]);
        console.log(successInput);
        if (successInput) {
            let errorMessage = verificationInput.changeMessage(nameInput, 'register');
            this.setState({
                errorInputMessage: errorMessage,
                registerCheck: {
                    [nameInput + 'Input']: successInput
                }

            })
        }
        return successInput

    }

    checkInput() {
        let success = true;
        let successLogin = this.checkSomeInput('email');
        let successPassword = this.checkSomeInput('password');
        let successEmail = this.checkSomeInput('login');
        if (successLogin || successPassword || successEmail) {
            success = false
        }
        return success
    }

    async sendUserInfo() {
        let success = this.checkInput();
        if (success) {
            let registerInfo = await fetchProviders.getUser(this.state.user, '/register');
            const {emailSuccess, loginSuccess, registerMsg} = registerInfo;
            const {login, img, email} = this.state.user;
            this.setState({
                registerCheck: {
                    loginInput: !loginSuccess,
                    passwordInput: false,
                    emailInput: !emailSuccess,
                },
                errorInputMessage: registerMsg
            });
            if (loginSuccess && emailSuccess) {
                this.props.changeAuth(true, {login, img, email});
                this.props.history.push('/');
            }
        }

    }

    render() {
        const {classes} = this.props;
        console.log(this.state);
        const {loginInput, emailInput, passwordInput} = this.state.registerCheck;
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Typography variant="title" className={classes.paper__typography}>
                        Оу, новенький, садись расскажи нам о себе!
                    </Typography>
                    <TextField
                        required
                        error={loginInput}
                        name="login"
                        label="Login"
                        placeholder="Your Login"
                        className={classes.textInput}
                        onChange={this.changeInput}
                    />
                    <TextField
                        required
                        error={passwordInput}
                        name="password"
                        label="Password"
                        placeholder="Your Password"
                        className={classes.textInput}
                        type="password"
                        onChange={this.changeInput}
                    />
                    <TextField
                        required
                        error={emailInput}
                        name="email"
                        label="Email"
                        placeholder="Your Email"
                        className={classes.textInput}
                        onChange={this.changeInput}
                    />
                    <TextField
                        name="number"
                        label="Number"
                        placeholder="Your Number"
                        className={classes.textInput}
                        onChange={this.changeInput}
                    /><TextField
                    name="img"
                    label="Image"
                    placeholder="Your Image"
                    className={classes.textInput}
                    onChange={this.changeInput}
                />
                    <Typography className={classes.paper__typography}>
                        {this.state.errorInputMessage}
                    </Typography>
                    <Button variant="contained" color="primary" onClick={this.sendUserInfo} className={classes.button}>
                        Время за покупками!
                    </Button>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Register)