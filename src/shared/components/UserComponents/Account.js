import React from 'react'

import fetchProviders from '../../providers/fetchProviders'
import verificationInput from '../../providers/verificationInput'

import {
    withStyles
} from '@material-ui/core/styles'

import {
    Paper,
    Typography,
    TextField,
    Button
} from '@material-ui/core'

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center'
    },
    paper: {
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        maxWidth: 550,
        height: 500
    },
    paper__typography: {
        textAlign: 'center',
        width: '100%',
        height: 50
    },
    paper__content: {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-start'
    },
    paper__img: {
        width: 200,
        height: 200,
        backgroundSize: '100%'
    },
    paper__textField: {
        width: 300,
        height: '100%',
        marginLeft: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },
    button: {
        margin: theme.spacing.unit
    }
});

class Account extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                login: '',
                password: '',
                email: '',
                number: '',
                img: ''
            },
            newUserInfo: {
                login: '',
                email: '',
                number: '',
                img: ''
            },
            sendUserData: {
                login: '',
                email: '',
                number: ''
            },
            empty: {
                login: false,
                email: false,
                number: false
            },
            success: false,
            successInput: false,
            errorMessage: ''
        }
        this.loadData = this.loadData.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.checkInput = this.checkInput.bind(this);
        this.sendData = this.sendData.bind(this)
        this.checkUpdate = this.checkUpdate.bind(this)
    }

    async loadData() {
        let userData = await fetchProviders.getUser(this.props.email, '/account');
        this.setState({
            user: {
                ...userData.data
            },
            success: userData.success
        })
    }

    componentDidMount() {
        this.loadData();
    }

    checkInput(nameInput) {
        let emptyInput = verificationInput.checkInput(this.state.newUserInfo[nameInput]);
        if (emptyInput) {
            return false
        } else {
            this.setState({
                sendUserData: {
                    ...this.state.sendUserData,
                    [nameInput]: this.state.newUserInfo[nameInput]
                }
            });
            return true
        }

    }

    async checkUpdate(nameData) {
        let successInput = await this.checkInput(nameData);
        if(successInput){
            let sendInfo = {
                name: nameData,
                data: this.state.sendUserData[nameData]
            }
            console.log(sendInfo);
            let data = await fetchProviders.getUser(sendInfo, '/changeAccount');
            const {success, errorMessage} = data;
            this.setState({
                empty:{
                    [nameData]:!success
                },
                errorMessage
            })
            return success
        }
        return successInput;
    }

    async sendData() {
        let successLogin = this.checkUpdate('number');
        let successEmail = this.checkUpdate('email');
        let successNumber = this.checkUpdate('login');
    }

    changeInput(event) {
        const {name, value} = event.target
        this.setState({
            newUserInfo: {
                ...this.state.newUserInfo,
                [name]: value
            }
        })
    }

    render() {
        console.log(this.props.email);
        console.log(this.state);
        const {login, email, number, img} = this.state.user;
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Typography className={classes.paper__typography}>
                        Ты настолько нас впечатлил, что мы решили записать всё о тебе, хочешь что-то подправить?
                    </Typography>
                    <div className={classes.paper__content}>
                        <img
                            src={img}
                            className={classes.paper__img}
                        />
                        <div className={classes.paper__textField}>
                            <TextField
                                label={'Login: ' + login}
                                name="login"
                                placeholder={login}
                                fullWidth
                                error={this.state.empty.login}
                                onChange={this.changeInput}
                            />
                            <TextField
                                label={'Email: ' + email}
                                name="email"
                                placeholder={email}
                                fullWidth
                                onChange={this.changeInput}
                                error={this.state.empty.email}
                            />
                            <TextField
                                label={'Number: ' + number}
                                name="number"
                                placeholder={number}
                                fullWidth
                                onChange={this.changeInput}
                                error={this.state.empty.number}
                            />
                            <Typography className={classes.paper__typography}>{this.state.errorMessage}</Typography>
                            <Button className={classes.button} onClick={this.sendData} variant="contained"
                                    color="primary">
                                Вот так правильно
                            </Button>
                        </div>
                    </div>
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(Account)