import React from 'react'
import {
    Link
} from 'react-router-dom'

import fetchProviders from '../../providers/fetchProviders'
import verificationInput from '../../providers/verificationInput'

import {
    withStyles
} from '@material-ui/core/styles'

import {
    Paper,
    Typography,
    TextField,
    Button
} from '@material-ui/core'

const styles = theme => ({
    root: {
        display: 'flex',
        justifyContent: 'center'
    },
    paper: {
        maxWidth: 300,
        height: 450,
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    paper__alignText: {
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        textAlign: 'center'
    },
    textInput: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
        height: 60
    },
    button: {
        margin: theme.spacing.unit,
        marginBot:20
    }


})

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user:{
                login: '',
                password: '',
            },
            loginCheck:{
                loginInput:false,
                passwordInput:false
            },
            errorInputMessage:''
        }
        this.changeInput = this.changeInput.bind(this);
        this.sendLogin=this.sendLogin.bind(this);
        this.checkInput=this.checkInput.bind(this);
        this.checkSomeInput = this.checkSomeInput.bind(this)
    }

    checkSomeInput(nameInput){
        let successInput = verificationInput.checkInput(this.state.user[nameInput]);
        console.log(successInput);
        if(successInput){
            let errorMessage = verificationInput.changeMessage(nameInput,'login');
            console.log(errorMessage)
            this.setState({
                loginCheck:{
                    [nameInput + 'Input']:successInput
                },
                errorInputMessage:errorMessage
            })
        }
        return successInput

    }

    checkInput(){
        let success = true;
        let successLogin = this.checkSomeInput('password');
        let successPassword = this.checkSomeInput('login')
        if(successPassword || successLogin){
            success = false
        }
        return success;
    }

    changeInput(event) {
        const {name, value} = event.target;
        this.setState({
            user:{
                ...this.state.user,
                [name]: value
            }
        })
    }

    async sendLogin(){
        console.log(this.state);
        let success = this.checkInput();
        if(success){
            let auth = await fetchProviders.getUser(this.state.user,'/login');
            await this.setState({
                loginCheck:{
                    loginInput:false,
                    passwordInput:false
                },
                errorInputMessage:auth.msg
            })
            const {login,img,email,products} = auth.data;
            console.log(products);
            this.props.changeAuth(auth.success,{login,img,email,products});
            this.props.history.push('/');
        }
    }

    render() {
        const classes = this.props.classes;
        const {loginInput,passwordInput} = this.state.loginCheck
        console.log(this.state)
        return (
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Typography variant="title" className={classes.paper__alignText}>
                        Опаздываешь! Без тебя все раскупят
                    </Typography>
                    <TextField
                        required
                        error={loginInput}
                        name="login"
                        label="Login"
                        placeholder="Your Login"
                        className={classes.textInput}
                        onChange={this.changeInput}
                    />
                    <TextField
                        required
                        error={passwordInput}
                        name="password"
                        label="Password"
                        placeholder="Your Password"
                        className={classes.textInput}
                        type="password"
                        onChange={this.changeInput}
                    />
                    <Button variant="contained" color="primary" className={classes.button} onClick={this.sendLogin}>
                        Уже иду!
                    </Button>
                    <Typography className={classes.paper__alignText}>
                        {this.state.errorInputMessage}
                    </Typography>
                    <Typography className={classes.paper__alignText}>
                        Мы ещё не знакомы? Тогда присаживайся, расскажи нам свою историю
                        <Link to='/register'> Регистрация</Link>
                    </Typography>
                </Paper>
            </div>)
    }
}

export default withStyles(styles)(Login)