import React from 'react'

import {
    withStyles
} from '@material-ui/core/styles'

import {
    Link
} from 'react-router-dom'

import {
    Paper,
    Typography
} from '@material-ui/core'

const styles = theme =>({
    root:{
        display:'flex',
        justifyContent:'center'
    },
    paper:{
        width:300,
        height:400,
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display:'flex',
        justifyContent:'center',
        flexWrap:'wrap'
    },
    paper__typography:{
        textAlign:'center'
    },
    paper__img:{
        width:200,
        height:200,
        marginTop:10
    }
})

class NoAuthAccount extends React.Component{
    render(){
        const {classes} = this.props;
        return(
            <div className={classes.root}>
                <Paper className={classes.paper}>
                    <Typography variant="title" className={classes.paper__typography}>
                        Не ожидал тебя здесь увидеть.Прости, но мы пускаем сюда только своих, для начала <Link to='/login'>представся </Link>
                    </Typography>
                    <img className={classes.paper__img} src ="https://stflorian.hu/wp-content/uploads/2015/12/2allj-elsobbsegadas_kotelezo_800x800.jpg" />
                </Paper>
            </div>
        )
    }
}

export default withStyles(styles)(NoAuthAccount)