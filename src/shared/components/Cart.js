import React from 'react'
import {
    withStyles
} from '@material-ui/core/styles'
import fetchProviders from "../providers/fetchProviders";

import {
    Typography,
    Paper,
    Grid,
    Card,
    CardMedia,
    CardContent
}
from "@material-ui/core"

const styles = theme =>({
    root:{
        display:'flex',
        justify:'center'
    },
    grid:{
        display:"flex",
        flexWrap:'wrap'
    },
    card:{
        height:50,
        width:"90%",
        display:'flex',
        flexDirection: 'column'
    },
    cardMedia: {
        height: 300,
        backgroundSize: '100%'
    },
})

class Cart extends React.Component{

    constructor(props){
        super(props)
        this.state={
            products:[],
            success:false,
            errorMessage:''

        }
        this.loadProducts = this.loadProducts.bind(this);
    }

    async loadProducts(){
        let productsData = await fetchProviders.getUser(this.props.email, '/cart');
        this.setState({
            ...productsData
        })
    }

    componentDidMount(){
        this.loadProducts()
    }

    render(){
        console.log("Hello cart");
        console.log(this.props.email);
        console.log(this.props.match.url);
        console.log(this.state);
        const {classes} = this.props;
        return(
            <div
                className={classes.root}
            >
                {this.state.products.map((product)=>{
                    return <Grid className={classes.grid}>
                        <Card key={product._id} className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image={product.img}/>
                            <CardContent>
                                <Typography>
                                    {product.name}
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                })}
            </div>
        )
    }
}

export default withStyles(styles)(Cart)