import React from 'react'

import {
    withStyles
} from '@material-ui/core/styles';


import Home from './Home'
import ProductsContainer from '../containers/ProductsContainer'
import AccountContainer from '../containers/AccountContainer'
import Login from './UserComponents/Login'
import Register from './UserComponents/Register'
import Cart from './Cart'

import {
    Switch,
    Route
} from 'react-router-dom'

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justify: 'flex-start',
        overflow: 'hidden',
    }
})


class Content extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        const {classes} = this.props;
        return (
            <Switch className={classes.root}>
                <Route exact path='/' component={Home}/>
                <Route path='/login' render={(routeProps) => <Login
                    changeAuth={(authState, userData) => {
                        this.props.changeAuth(authState, userData)
                    }}
                    {...this.props}
                    {...routeProps}
                />}
                />
                <Route path='/register' render={(routeProps) => <Register
                    changeAuth={(authState, userData) => {
                        this.props.changeAuth(authState, userData)
                    }}
                    {...this.props}
                    {...routeProps}
                />}/>
                <Route path='/account' render={() => <AccountContainer
                    email={this.props.email}
                    isAuth={this.props.isAuth}
                />}
                />
                <Route path='/cart' render={(routeProps) => <Cart
                    {...this.props}
                    {...routeProps}
                />}
                />
                <Route path='/:products' render={(routeProps) => <ProductsContainer
                    {...this.props}
                    {...routeProps}
                />}
                />
            </Switch>
        )

    }
}

export default withStyles(styles)(Content)