import React from 'react'

import {
    Link
} from 'react-router-dom'

import {
    AccountCircle
} from '@material-ui/icons'

import {
    IconButton,
    Typography
} from '@material-ui/core'

import {
    withStyles
} from '@material-ui/core/styles'

const styles = theme => ({
    root: {
        width: 110,
        height: 40,
        display: 'flex',
        alignItems:'center'
    },
    login__button: {
        textDecoration: 'none',
        color: 'white',
        margin: 1
    },
})

class LoginIcon extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <IconButton color="inherit">
                    <Link to='/login' className={classes.login__button}>
                        <AccountCircle/>
                    </Link>
                </IconButton>
                <Typography color="inherit">Login</Typography>
            </div>
        )
    }
}

export default withStyles(styles)(LoginIcon)