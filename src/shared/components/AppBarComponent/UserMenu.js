import React from 'react'
import {Link} from 'react-router-dom'

import {
    withStyles
} from '@material-ui/core/styles'

import defaultAvatar from '../../../../dist/assets/defaultAvatar.png'


import {
    ExpandMore
} from '@material-ui/icons'


import {
    Avatar,
    Menu,
    MenuItem,
    MenuList,
    Typography,
    IconButton,
    ClickAwayListener
} from '@material-ui/core'

const styles = theme => ({
    root: {
        width: 150,
        height: 40,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    avatar: {
        width: 30,
        height: 30,
        margin: 5,
        border: '1px solid white'
    },
    button: {
        height: 30,
        width: 30
    },
    loginMenu: {
        transformOrigin: 'center bottom',
        textDecoration:'none'
    },
    menuLink: {
        textDecoration: 'none',
        color: 'black'
    }
})

class UserMenu extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user: {
                login: '',
                img: defaultAvatar,
                email: ''
            },
            menuStatus: false,
            anchorEl: null,
            exitStatus: false
        }
        this.menuClose = this.menuClose.bind(this);
        this.menuOpen = this.menuOpen.bind(this);
        this.exitAccount = this.exitAccount.bind(this)
    }

    componentDidMount() {
        this.setState({
            user: {
                ...this.props.userData
            }
        });
        if (!!this.state.user.img) {

        } else {
            this.setState({
                user: {
                    img: defaultAvatar
                }
            })
        }
    }

    menuClose() {
        this.setState({
            anchorEl: null
        })
    }

    exitAccount() {
        const {exitStatus} = this.state;
        this.props.exitAccount(exitStatus)
    }


    menuOpen(event) {
        console.log(event.currentTarget);
        this.setState({
            anchorEl: event.currentTarget
        })
    }

    render() {
        const {classes} = this.props;
        const {img, login} = this.state.user
        const {anchorEl} = this.state
        console.log(this.state);
        console.log(this.props);
        return (
            <div className={classes.root}>
                <Avatar src={img} className={classes.avatar}/>
                <Typography color="inherit">{login}</Typography>
                <IconButton color="inherit" className={classes.button}>
                    <ExpandMore
                        aria-owns={anchorEl ? 'menu-list-grow' : null}
                        onClick={this.menuOpen}/>
                </IconButton>
                <ClickAwayListener onClickAway={this.menuClose}>
                    <Menu
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={this.menuClose}
                        className={classes.loginMenu }
                    >

                        <Link to='/account' className={classes.menuLink}>
                            <MenuItem className={classes.menuLink} onClick={this.menuClose}>
                                Личный кабинет
                            </MenuItem>
                        </Link>
                        <MenuItem onClick={this.exitAccount}>Выход</MenuItem>
                    </Menu>
                </ClickAwayListener>
            </div>
        )
    }
}

export default withStyles(styles)(UserMenu)