import fetch from 'isomorphic-fetch'


class fetchProviders {

    static async getUser(data,uri) {
        const url = `http://localhost:8080${uri}`;
        let response = await fetch(url, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                user: data
            })
        });
        let product = response.json();
        return product;
    }
    static async getRequest(uri){
        let dataUrl = `http://localhost:8080${uri}`;
        let response = await fetch(dataUrl);
        let data = response.json();
        return data;
    }

}

export default fetchProviders