class verificationInput {
    static checkInput(input) {
        let success = true;
        if (!!input) {
            success = false
        }
        return success
    }

    static changeMessage(nameInput, nameComponent) {
        let errorMessage = '';
        switch (nameComponent) {
            case 'login': {
                switch(nameInput){
                    case 'login':{
                        errorMessage='Прости, я не разслышал как тебя зовут?';
                        break;
                    }
                    case 'password':{
                        errorMessage='Не уверен, что это ты, может скажешь слово которое знаем только мы?';
                        break;
                    }
                }
                break;
            }
            case 'register': {
                switch (nameInput) {
                    case 'login': {
                        errorMessage = 'Кажется ты забыл представится';
                        break;
                    }
                    case 'password': {
                        errorMessage = 'Оу, как же я смогу тебя узнать при нашей следуйщей встрече?';
                        break;
                    }
                    case 'email': {
                        errorMessage = 'Куда же я буду отсылать письма?';
                        break;
                    }
                }
            }
        }
        return errorMessage
    }
}

export default verificationInput
