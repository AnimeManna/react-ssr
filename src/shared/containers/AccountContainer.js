import React from 'react'

import Account from '../components/UserComponents/Account'
import NoAuthAccount from '../components/UserComponents/NoAuthAccount'


class AccountContainer extends React.Component{
    render(){
        console.log(this.props);
        return(
            <div>
                {this.props.isAuth
                    ? <Account email={this.props.email} />
                    : <NoAuthAccount />
                }
            </div>
        )
    }
}

export default AccountContainer