import React from 'react'

import Products from '../components/Products'
import Product from '../components/Product'

import {Route,Switch} from 'react-router-dom'

class ProductsContainer extends React.Component{
    render(){
        console.log(this.props)
        return(
            <div>
                <Switch>
                    <Route exact path = '/:products' component={Products} />
                    <Route path='/:products/:id' render={(routeProps)=><Product
                        {...this.props}
                        {...routeProps}
                    />}/>
                </Switch>

            </div>
        )
    }
}

export default ProductsContainer